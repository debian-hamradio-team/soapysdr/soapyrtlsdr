soapyrtlsdr (0.3.3-1) unstable; urgency=medium

  * New upstream version 0.3.3
  * Bump Standards-Version to 4.6.1, no changes required

 -- Andreas Bombe <aeb@debian.org>  Tue, 25 Oct 2022 01:35:59 +0200

soapyrtlsdr (0.3.2-1) unstable; urgency=medium

  * New upstream version 0.3.2
  * Adapt d/watch to GitHub release list changes and make it accept
    soapy-rtl-sdr tag names in addition to soapy-rtlsdr
  * Remove --as-needed linker option, it is the default now
  * Use https for format field URL in d/copyright

 -- Andreas Bombe <aeb@debian.org>  Sun, 30 Jan 2022 18:34:07 +0100

soapyrtlsdr (0.3.0-4) unstable; urgency=medium

  * Upload to unstable

 -- Andreas Bombe <aeb@debian.org>  Tue, 07 Sep 2021 01:29:16 +0200

soapyrtlsdr (0.3.0-3) experimental; urgency=medium

  * Change SoapySDR ABI version in package names and dependencies from 0.7 to
    0.8
  * Set debhelper compat level in Build-Depends
  * Raise debhelper compat level to 13
  * Bump Standards-Version to 4.6.0, no changes required
  * Add Rules-Requires-Root field to d/control

 -- Andreas Bombe <aeb@debian.org>  Fri, 27 Aug 2021 01:48:09 +0200

soapyrtlsdr (0.3.0-2) unstable; urgency=medium

  * Upload to unstable
  * Bump Standards-Version to 4.4.1, no changes required

 -- Andreas Bombe <aeb@debian.org>  Sun, 20 Oct 2019 17:43:42 +0200

soapyrtlsdr (0.3.0-1) experimental; urgency=medium

  * New upstream version 0.3.0
  * Change SoapySDR ABI version in package names and dependencies from 0.6 to
    0.7
  * Raise debhelper compat level to 12
  * Bump Standards-Version to 4.4.0, no changes required

 -- Andreas Bombe <aeb@debian.org>  Thu, 12 Sep 2019 22:34:15 +0200

soapyrtlsdr (0.2.5-1) unstable; urgency=medium

  * New upstream version 0.2.5
  * Raise debhelper compat level to 11
  * Change Vcs-* fields in debian/control to the salsa repository addresses
  * Link with --as-needed
  * Include /usr/share/dpkg/default.mk in debian/rules instead of defining
    DEB_HOST_MULTIARCH ourselves
  * Bump Standards-Version to 4.1.4, no changes required

 -- Andreas Bombe <aeb@debian.org>  Thu, 10 May 2018 15:26:17 +0200

soapyrtlsdr (0.2.4-1) unstable; urgency=medium

  * New upstream version 0.2.4
  * Change SoapySDR ABI version in package names and dependencies from 0.5-2
    to 0.6
  * Build-Depend on versioned libsoapysdr0.6-dev instead of libsoapysdr-dev
  * Add Josh Blum copyright line to debian/copyright
  * Bump Standards-Version to 4.0.1, no changes required

 -- Andreas Bombe <aeb@debian.org>  Sun, 06 Aug 2017 15:26:15 -0400

soapyrtlsdr (0.2.2-2) unstable; urgency=medium

  * Make soapysdr-module-rtlsdr Multi-Arch: same
  * Raise debhelper compat level to 10
  * Remove unnecessary dh_installchangelogs override

 -- Andreas Bombe <aeb@debian.org>  Tue, 10 Jan 2017 03:55:20 +0100

soapyrtlsdr (0.2.2-1) unstable; urgency=medium

  * New upstream version 0.2.2
  * Remove implement-big-endian-build patch, applied upstream
  * Add FindRTLSDR.cmake entry to debian/copyright file

 -- Andreas Bombe <aeb@debian.org>  Mon, 12 Sep 2016 01:22:23 +0200

soapyrtlsdr (0.2.1-2) unstable; urgency=medium

  * Add implement-big-endian-build patch to fix build failure on all big
    endian architectures

 -- Andreas Bombe <aeb@debian.org>  Tue, 16 Aug 2016 22:10:42 +0200

soapyrtlsdr (0.2.1-1) unstable; urgency=medium

  * Initial release (Closes: #834378)

 -- Andreas Bombe <aeb@debian.org>  Tue, 16 Aug 2016 03:39:42 +0200
